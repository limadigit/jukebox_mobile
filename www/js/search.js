window.onerror = function(message, url, lineNumber) {
    //alert("Error: " + message + " in " + url + " at line " + lineNumber);
}
$('.tabs').show();
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var messageCount=0;
        var countRequest=0;
        var tooFar=false;
        var lastId=0;
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        if(isLogin=='true') {
            if(locationAdmin!='' && locationAdmin!=null){
                var myApp = new Framework7();
                var $$ = Dom7;
                var userid=storage.getItem('user_id');
                var playlist_id=storage.getItem('playlist_id');
                var username=storage.getItem('username');
                var email=storage.getItem('email');
                var myImage=storage.getItem('image');
                var varLoc=storage.getItem('location_id');
                var latlongTemp=storage.getItem('latlong').split('#');
                var lat=latlongTemp[0];
                var long=latlongTemp[1];
                var mySearchbar = myApp.searchbar('.searchbar', {
                    searchList: '.list-block-search',
                    searchIn: '.item-title'
                });
                $$('#search-input').on('change',function (e) {
                    var params={};
                    params.token=storage.getItem('token');
                    params.loc=storage.getItem('location_id');
                    params.q=mySearchbar.query;
                    window.plugins.spinnerDialog.show(null,"Loading ...", true);
                    $$.get(search_url+'?q='+mySearchbar.query+'&loc='+storage.getItem('location_id'),params, function (data) {
                        window.plugins.spinnerDialog.hide();
                        var response=JSON.parse(data);
                        if(response.status){
                            var html="";
                            // var onclick2 = alert("kliked");
                            $$('.searchbar-not-found').hide();
                            $$('.searchbar-found').show();
                            for(var i=0;i<response['length'];i++){
                                var from="";
                                // var onclick2 = 'selectDB("'+response[i].track_id+'","'+response[i].link+'","'+response[i].title+'")';
                                    
                                if(response[i].link.match(/youtube/)){
                                    var icon = "<i class='fa fa fa-youtube-play fa-2x' style='color:#bb0000'></i>";
                                    from="youtube";
                                }else{
                                    var icon = "<i class='fa fa-soundcloud fa-2x' style='color:#ff3a00'></i>";
                                    from="soundcloud";
                                }
                                if(i%2==0){
                                    html += "<li class='item-content' data-title='"+response[i].title+"' data-songid='"+response[i].track_id+"' data-from='"+from+"' data-loc='' data-genre='"+response[i].genre+"' data-link='"+response[i].link+"' style='border-bottom:1px solid #d2d2d2;background-color:#f5f5f5'>";
                                }else{
                                    html += "<li class='item-content' data-title='"+response[i].title+"' data-songid='"+response[i].track_id+"' data-from='"+from+"' data-loc='' data-genre='"+response[i].genre+"' data-link='"+response[i].link+"' style='border-bottom:1px solid #d2d2d2'>";
                                }

                                html += "<div class='item-media' style='min-width:30px;max-width:30px'>"+icon+"</div>";
                                html += "<div class='item-inner'><div class='item-title'>"+response[i].title+"</div>";
                                html += "<div class='item-after' style='text-align:center'><i  class='fa fa-play-circle fa-2x' style='color: #BCD8DD;margin-right: 28px;height: 36px;width: 44px;background: none ;margin-top: -11px;padding-top: 13px;'></i></div>";
                                html += "</div></li>";

                                // html += "<button style='position:relative; z-index:99; margin-top:-37px; margin-right:10px;  float:right; background-color:red; color:white;  border:none; border-radius:180px; width:25px; height:25px;'  data-title='"+response[i].title+"' data-songid='"+response[i].track_id+"' data-from='"+from+"' data-loc='' data-genre='"+response[i].genre+"' data-link='"+response[i].link+"'> <i class='fa fa-plus'></i></button>";
                                html += "<button style='position:relative; margin-top:-49px;   float:right; background:none; color:#FF8E6C;  border:none;  width:44px; height:49px;'  data-title='"+response[i].title+"' data-songid='"+response[i].track_id+"' data-from='"+from+"' data-loc='' data-genre='"+response[i].genre+"' data-link='"+response[i].link+"'> <i class='fa fa-plus-circle fa-2x'></i></button>";
                                

                            }
                            $$('#searchResult').html(html);
                        }else{
                            $$('.searchbar-not-found').html(response.errMobile)
                            $$('.searchbar-not-found').show();
                            $$('.searchbar-found').hide();
                        }
                    },function (error) {
                        window.plugins.spinnerDialog.hide();
                        alert("Network Error");
                    });
                });
                $$('#currentSong').hide();
                function initCount() {
                    if(messageCount>0){
                        $$('#messageCount').html(messageCount);
                        $$('#messageCount').show();
                    }else{
                        $$('#messageCount').hide();
                    }
                }
                initCount();
                $$('#search-btn').on('click',function (e) {
                    $$('#menu').hide();
                    $$('#search-form').show();
                });
                $$('#back-btn').on('click',function (e) {
                    history.back();
                });
                try{
                    var socket = io.connect('http://ws.jukebox5d.com/');
                    socket.on("connect",function() {
                        var params={username: username, loc: varLoc, userid: userid};
                        socket.emit("jukeboxLocation", varLoc);
                        socket.emit("getChatFileWeb",params);
                        socket.emit("adduser",params);
                        socket.emit("getRecentSong",{userid: userid, loc: varLoc});
                    });
                    socket.on("playlist",function(resp) {
                        var resp=JSON.parse(resp);
                        var medal="";
                        // alert(JSON.stringify(resp));
                        if(resp.status){
                            //first song
                            if(resp.data.length>0){
                                countRequest=0;
                                if(resp.data[0]){
                                    lastId=resp.data[0].user_id;
                                    if(resp.data[0].user_id==userid) {
                                        countRequest++;
                                    }
                                }
                                for(var i=1;i<resp.data.length;i++){
                                    lastId=resp.data[i].user_id;
                                    if(resp.data[i].user_id==userid) {
                                        countRequest++;
                                    }
                                }
                            }else{
                                countRequest=0;
                                lastId=0;
                            }
                        }
                    });
                }catch (err){
                }
                function jukeDialog(data,reload) {
                    if(countRequest>=2 || lastId==userid || tooFar){
                        if(countRequest>=2 || lastId==userid){
                            myApp.alert("It's not your turn yet. Invite your friend to insert a song first. (max. 2 songs per user)",'Jukebox5D');
                        }else{
                            myApp.alert("Too Far From Admin",'Jukebox5D');
                        }
                    }else{

                        myApp.confirm('Add this song to playlist? <br/> '+data.title,'Jukebox 5D',
                            function () {
                                myApp.prompt('Tell us how you feel about this song!','Jukebox 5D', function (value) {
                                    data.cap=encodeURIComponent(value);
                                    socket.emit("insertSong",data);
                                    myApp.alert('Wait until it\'s played.','Jukebox5D',function () {
                                        document.location.href='index.html';
                                    });
                                countRequest++;

                                });
                                lastId=userid;
                            },

                        );
                        // navigator.notification.confirm(
                        //     "Yuhu, lagu "+data.title+" sudah terpilih, mau di apakan lagunya?", // the message
                        //     function( index ) {
                        //         switch ( index ) {
                        //             case 1:
                        //                myApp.confirm('Add this song to playlist? <br/> '+data.title,'Jukebox 5D',
                        //                     function () {
                        //                         myApp.prompt('Tell us how you feel about this song!','Jukebox 5D', function (value) {
                        //                             data.cap=encodeURIComponent(value);
                        //                             socket.emit("insertSong",data);
                        //                             myApp.alert('Wait until it is played.','Jukebox5D',function () {
                        //                                 if(reload){
                        //                                     location.reload();
                        //                                 }
                        //                             });
                        //                         });
                        //                         lastId=userid;
                        //                         countRequest++;
                        //                     },
                        //                     function () {

                        //                     }
                        //                 );
                        //                 break;
                        //             case 2:
                        //                 myApp.popup('.popup-library');
                                     
                        //                 break;
                        //             case 3:
                        //                 // The third button was pressed
                        //                 break;
                        //         }
                        //     },
                        //     "Jukebox5D",                   // a title
                        //     [ "Mainkan Lagu", "Tambah ke Library", "Batal" ] // text of the buttons
                        // );


                    }
                }
                function test(){

                    alert("this is function test!");
                }

                $$('#user-playlist-popup').on('click', function (e) {
                    location.href="user_playlist.html";
                });
                $$(document).on('click', '#searchResult li', function (e) {
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.loc=varLoc;
                    data.from=$$(this).data('from');
                    data.link=$$(this).data('link');
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=1;
                    jukeDialog(data,true);
                });

                 getPlaylist();

                $$(document).on('click', '#searchResult button', function () {
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.loc=varLoc;
                    data.from=$$(this).data('from');
                    data.link=$$(this).data('link');
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=1;
                    document.getElementById('id-library').setAttribute('data-title', data.title);
                    document.getElementById('id-library').setAttribute('data-link', data.link);
                    document.getElementById('id-library').setAttribute('data-songid', data.id);
                    // getPlaylist();
                    myApp.popup('.popup-library');
                    // getPlaylist();
                    
                    // selectDB(data.id,data.link,data.title);

                });


    //             $('select #library-id').on('change', function() {
				// 	alert( this.value );
				// });

                $$(document).on('click', '.button-library', function () {
                    // getPlaylist();
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.track_id=$('.button-library').data('songid');
                    data.title=$('.button-library').data('title');
                    data.link=$('.button-library').data('link');
                    data.playlist_id=$('#library-id option:selected').val();
                    // alert(""+data.playlist_id+"|"+data);
                    // alert("this is live search");

                     selectDB(data);


                    // alert("this is id : "+data.id+"this is link : "+data.link+"this is title: "+data.title + "this is playlist id :" +playlist_id);



                    // selectDB(data.id,data.link,data.title);


                        
                });
                // getPlaylist(); 
                function getPlaylist() {
                    var params={};
                    params.token=storage.getItem('token');
                    $$.get(get_playlist,params, function (data) {
                        var response=JSON.parse(data);
                        if(response.status){
                        	var html="";

                 
                            for (var i=0;i<response.data.length;i++){

                                   //  html+=' <li> <a href="#"  data-playlistname="'+response.data[i].playlist_name+'" data-playlistid="'+response.data[i].playlist_id+'" class="item-link item-content">' +
                                   // '<div class="item-inner">'+
                                   // '<div class="item-title">'+response.data[i].playlist_name+'</div>'+
                                   // '</div></a>'+
                                   // '</li>nan';

                                   $("#library-id > option").ready(function(){
										html+= '<option value = "'+response.data[i].playlist_id+'"> '+response.data[i].playlist_name+' </option>';
										// html += response.data[i].playlist_name;
									});

                            }
                          $$('#library-id').html(html);
                        }
                    },function (error) {
                        alert("Network Error");
                    });
                }
                


                function onSuccess(position) {
                    var distance=showDistanceFromServer(position.coords.latitude,position.coords.longitude);
                    if(distance>1000){
                        tooFar=true;
                    }else{
                        tooFar=false;
                    }
                }
                function showDistanceFromServer(latitude, longitude) {
                    var lat2 = lat;
                    var lon2 = long;
                    var R = 6378137; // Radius of the earth in m
                    var dLat = deg2rad(lat2-latitude);  // deg2rad below
                    var dLon = deg2rad(lon2-longitude);
                    var a =
                            Math.sin(dLat/2) * Math.sin(dLat/2) +
                            Math.cos(deg2rad(latitude)) * Math.cos(deg2rad(lat2)) *
                            Math.sin(dLon/2) * Math.sin(dLon/2)
                        ;
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    var d = R * c; // Distance in m
                    console.log(d);
                    return d.toFixed(1);
                }
                function deg2rad(deg) {
                    return deg * (Math.PI/180)
                }

                function onError(error) {
                    console.log('code: '    + error.code    + '\n' +
                        'message: ' + error.message + '\n');
                }

                function selectDB(data){
                        data.token=storage.getItem('token');
                        window.plugins.spinnerDialog.show(null, "Loading ...", true);
                        $$.post(add_item_playlist, data, function (response) {
                        window.plugins.spinnerDialog.hide();
                        var json=JSON.parse(response);
                        if(json.status){
                            myApp.alert(json.message,"Jukebox5D");
                        }else{
                            myApp.alert(json.message,"Jukebox5D");
                        }
                            document.getElementById("tombol-tutup-popup").click();
                            location.href="index.html";

                    }, function (error) {
                        alert("Network Error");
                        window.plugins.spinnerDialog.hide();
                    });
                }



                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationAvailable(function (available) {
                        if (available) {
                            navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });
                        } else {
                            myApp.alert("Turn On GPS And Restart App !", 'Jukebox5D',function () {
                                cordova.plugins.diagnostic.switchToLocationSettings();

                            });
                        }
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }

            }else{
                location.href="choose.html";
            }
        }else{
            location.href="login.html";
        }
    }
};

app.initialize();

/**
 * Created by mrkai303 on 02/12/16.
 */
var url="https://jukebox5d.com/app/api.php/";
var login_url=url+"login";
var register_url=url+"register";
var forgot_url=url+"forgot";
var choose_url=url+"User/getsAdmin";
var search_url=url+"LiveSearch";
var sosmed_url=url+"User/SosmedLogin";
var profile_url=url+"User/Detail";
var follow_url=url+"User/follow";
var unfollow_url=url+"User/unfollow";
var music_url=url+"User/get_genre";
var password_url=url+"User/change_password";
var image_api=url+"GenerateImage";
var save_playlist=url+"User/savePlaylist";
var get_playlist=url+"User/getPlaylist";
var delete_playlist=url+"User/deletePlaylist";
var update_playlist=url+"User/updatePlaylist";
var add_item_playlist=url+"User/addItem";
var delete_item_playlist=url+"User/deleteItem";
var get_item_playlist=url+"User/getPlaylistUserItem";
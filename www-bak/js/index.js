window.onerror = function(message, url, lineNumber) {
    //alert("Error: " + message + " in " + url + " at line " + lineNumber);
}
//test update
$('.tabs').show();
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var title = "";
        var track_id = "";
        var link = "";
        var currentUserIDProfile=0,currentUsernameProfile="";
        var messageCount=0;
        var lastDate="";
        var isChat=false;
        var countRequest=0;
        var tooFar=false;
        var skipAble=true;
        var lastId=0;
        var currentPlaylistId=0;
        var currentTitle=0;
        var currentUser="";
        var currentLink="";
        var userReply="";
        var currentCaption="";
        var storage = window.localStorage;
        var mostRequest= new Array();
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        var assetUrl='https://jukebox5d.com/app/assets/';
        if(isLogin=='true') {
            if(locationAdmin!='' && locationAdmin!=null){
                var myApp = new Framework7();
                var $$ = Dom7;
                $$('#location-name').html(locationAdmin);
                var userid=storage.getItem('user_id');
                var username=storage.getItem('username');
                var email=storage.getItem('email');
                var myImage=storage.getItem('image');
                var varLoc=storage.getItem('location_id');
                var latlongTemp=storage.getItem('latlong').split('#');
                var lat=latlongTemp[0];
                var long=latlongTemp[1];
                var mainView = myApp.addView('.view-main', {domCache: true});
                var myMessagebar = myApp.messagebar('.messagebar', {
                    maxHeight: 200
                });
                var myMessages = myApp.messages('.messages', {});
                $$('#currentSong').hide();
                $$('#home-btn').click();
                function initCount() {
                    if(messageCount>0){
                        $$('#messageCount').html(messageCount);
                        $$('#messageCount').show();
                    }else{
                        $$('#messageCount').hide();
                    }
                }
                initCount();
                function isValidURL(url){
                    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                    if(RegExp.test(url)){
                        return true;
                    }else{
                        return false;
                    }
                }
                $$('#search-btn').on('click',function (e) {
                    location.href="search.html";
                });
                $$('#playlist-btn').on('click',function (e) {
                    location.href="user_playlist.html";
                });
                function chatInsert(y) {
                    console.log("===Chat Insert for My Id "+userid+" ===");
                    console.log(y);
                    var chat="";
                    var date = new Date(y.created);
                    var monthNames = ["January", "February", "March", "April", "May", "June",
                        "July", "August", "September", "October", "November", "December"
                    ];
                    var weekday=new Array(7);
                    weekday[0]="Sunday";
                    weekday[1]="Monday";
                    weekday[2]="Tuesday";
                    weekday[3]="Wednesday";
                    weekday[4]="Thursday";
                    weekday[5]="Friday";
                    weekday[6]="Saturday";
                    var day=weekday[date.getDay()];
                    var dd = date.getDate();
                    var mm = date.getMonth() + 1; //January is 0!
                    var yyyy = date.getFullYear();
                    var hours = date.getHours();
                    hours = (hours < 10 ? '0' : '') + hours;
                    var nowDate=day+' , '+dd+' '+monthNames[date.getMonth()]+' '+yyyy;
                    var minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();
                    if (isValidURL(y.images) != true) {
                        y.images = 'https://jukebox5d.com/app/assets/' + y.images;
                    }
                    var chatUser = Base64.decode(y.chat);
                    var splitReply = chatUser.split("•♪•");
                    if(lastDate!=nowDate){
                        lastDate=nowDate;
                        chat+='<div class="messages-date">'+nowDate+'</div>';
                    }
                    if(y.user_id==userid){
                        if(splitReply.length>2) {
                            chat+=' <div class="message message-sent">'+
                                '<div class="message-text to-reply" data-replyuser="'+username+'" data-reply="'+splitReply[2]+'"><div style="background-color: #eee; padding: 3px;"><b>'+splitReply[0]+'</b><br/>'+splitReply[1]+'</div>'+splitReply[2]+'<div class="message-date">'+hours+':'+minutes+'</div></div>'+
                                '</div>';
                        }else{
                            chat += '<div class="message message-sent"><div class="message-text to-reply" data-replyuser="'+username+'" data-reply="'+chatUser+'">'+chatUser+'<div class="message-date">'+hours+':'+minutes+'</div></div></div>';
                        }
                    }else{
                        if(splitReply.length>2) {
                            chat+=' <div class="message message-with-avatar message-received">'+
                                '<div class="message-name">'+y.username+'</div>'+
                                '<div class="message-text to-reply" data-replyuser="'+y.username+'" data-reply="'+splitReply[2]+'"><div style="background-color: #eee; padding: 3px;"><b>'+splitReply[0]+'</b><br/>'+splitReply[1]+'</div>'+splitReply[2]+'<div class="message-date">'+hours+':'+minutes+'</div></div>'+
                                '<div style="background-image:url('+y.images+')" class="message-avatar"></div>'+
                                '</div>';
                        }else{
                            chat+=' <div class="message message-with-avatar message-received">'+
                                '<div class="message-name">'+y.username+'</div>'+
                                '<div class="message-text to-reply" data-replyuser="'+y.username+'" data-reply="'+chatUser+'">'+chatUser+'<div class="message-date">'+hours+':'+minutes+'</div></div>'+
                                '<div style="background-image:url('+y.images+')" class="message-avatar"></div>'+
                                '</div>';
                        }
                    }
                    return chat;
                }
                function showProfile(user_id,username) {
                    storage.setItem('profile_user_id',user_id);
                    storage.setItem('profile_username',username);
                    location.href="profile.html";
                }
                // myApp.onPageBeforeRemove('search',function (p) {
                //     $$('#menu').show();
                // });
                $$('#currentUser-username').html(username);
                $$('#currentUser-email').html(email);
                if(isValidURL(myImage)){
                    $$('#currentUser-image').prop('src',myImage);
                }else{
                    $$('#currentUser-image').prop('src',assetUrl+myImage);
                }
                try{
                    var devicePlatform = device.platform;
                    var socket = io.connect('http://ws.jukebox5d.com/');
                    socket.on("connect",function() {
                        var params={username: username, loc: varLoc, userid: userid};
                        socket.emit("jukeboxLocation", varLoc);
                        socket.emit("getChatFileWeb",params);
                        socket.emit("adduser",params);
                        socket.emit("getRecentSong",{userid: userid, loc: varLoc});
                    });
                    socket.on("playlist",function(resp) {
                        var resp=JSON.parse(resp);
                        var medal="";
                        // alert(JSON.stringify(resp));
                        if(resp.status){
                            //first song
                            if(resp.data.length>0){
                                // title = resp.data[0].title;
                                // link = resp.data[0].link;
                                // track_id =resp.data[0].track_id ;
                                var like,dislike;
                                countRequest=0;
                                $$('#screenshot').show();
                                $$('#no-song').hide();
                                $$('#currentSong-img').removeClass("firstRank");
                                $$('#currentSong-img').removeClass("secondRank");
                                $$('#currentSong-img').removeClass("thirdRank");
                                $$('#currentSong-like').html('<i class="fa fa-heart fa-2x"></i><br/><small>Like</small> ');
                                $$('#currentSong-dislike').html('<i class="fa fa-fast-forward fa-2x"></i><br/><small>Skip</small> ');
                                $$('#addToLibrary').html('<i class="fa fa-plus fa-2x"></i><br/><small>Add to library risfa</small> ');


                                document.getElementById('addToLibrary').setAttribute('data-title', resp.data[0].title);
                                document.getElementById('addToLibrary').setAttribute('data-link', resp.data[0].link);
                                document.getElementById('addToLibrary').setAttribute('data-track_id', resp.data[0].track_id);

                                if(resp.data[0]){
                                    lastId=resp.data[0].user_id;
                                    $$('#currentSong').show();
                                    if(resp.data[0].user_id==userid) {
                                        countRequest++;
                                        $$("#currentSong-delete").show();
                                    }else {
                                        $$("#currentSong-delete").hide();
                                    }
                                    skipAble=true;
                                    currentPlaylistId=resp.data[0].playlist_id;
                                    if(resp.data[0].like != null) {
                                        var splitLike = resp.data[0].like.split("#");
                                        // splitLike.clean(" ");
                                        like = splitLike.length-1;
                                        var loopLove=true;
                                        $$('#currentSong-like').html('<i class="fa fa-heart fa-2x"></i><br/><small>'+like+' Like</small>');
                                        for(var i=0;i<splitLike.length;i++){
                                            if(loopLove) {
                                                if (splitLike[i] == userid) {
                                                    $$('#currentSong-like').removeClass('color-gray');
                                                    $$('#currentSong-like').addClass('color-red');
                                                    skipAble = false;
                                                    loopLove=false;
                                                } else {
                                                    $$('#currentSong-like').removeClass('color-red');
                                                    $$('#currentSong-like').addClass('color-gray');
                                                }
                                            }
                                        }
                                    }else{
                                        $$('#currentSong-like').removeClass('color-red');
                                        $$('#currentSong-like').addClass('color-gray');
                                    }
                                    if(resp.data[0].dislike != null) {
                                        var splitDislike = resp.data[0].dislike.split("#");
                                        // splitDislike.clean(" ");
                                        dislike = splitDislike.length-1;
                                        var loopSkip=true;
                                        $$('#currentSong-dislike').html('<i class="fa fa-fast-forward fa-2x"></i><br/><small>'+dislike+' Skip</small>');
                                        for(var i=0;i<splitDislike.length;i++){
                                            if(loopSkip) {
                                                if (splitDislike[i] == userid) {
                                                    $$('#currentSong-dislike').removeClass('color-gray');
                                                    $$('#currentSong-dislike').addClass('color-cyan');
                                                    skipAble = false;
                                                    loopSkip=false;
                                                } else {
                                                    $$('#currentSong-dislike').removeClass('color-cyan');
                                                    $$('#currentSong-dislike').addClass('color-gray');
                                                }
                                            }
                                        }
                                    }else {
                                        $$('#currentSong-dislike').removeClass('color-cyan');
                                        $$('#currentSong-dislike').addClass('color-gray');
                                    }
                                    var image = assetUrl+"images/user.jpg";
                                    if(isValidURL(resp.data[0].twitter_image)!=true){
                                        image=assetUrl+resp.data[0].twitter_image;
                                    }else{
                                        image=resp.data[0].twitter_image;
                                    }
                                    if(resp.data[0].user_id==mostRequest[0]){
                                        $$('#currentSong-img').addClass("firstRank");
                                        medal="<img src='img/gold.png' width='20'/>";
                                    }else if(resp.data[0].user_id==mostRequest[1]){
                                        $$('#currentSong-img').addClass("secondRank");
                                        medal="<img src='img/silver.png' width='20'/>";
                                    }else if(resp.data[0].user_id==mostRequest[2]){
                                        $$('#currentSong-img').addClass("thirdRank");
                                        medal="<img src='img/bronze.png' width='20'/>";
                                    }
                                    $$('#currentSong-img').data('userid',resp.data[0].user_id);
                                    $$('#currentSong-img').data('username',resp.data[0].createby);
                                    $$('#currentSong-delete').data('playlist_id',resp.data[0].playlist_id);
                                    $$('#currentSong-img').prop('src',image);
                                    $$('#currentSong-username').html('<b style="font-size:1.0em"> ' +medal+' '+resp.data[0].createby+'</b>');
                                    currentUser=resp.data[0].createby;
                                    $$('#currentSong-title').html(resp.data[0].title);
                                    currentTitle=resp.data[0].title;
                                    $$('#currentSong-caption').html(resp.data[0].caption);
                                    currentCaption=resp.data[0].caption;
                                    var from = "soundcloud";
                                    if(resp.data[0].link.match(/youtube/)) from = "youtube";
                                    if(from=="youtube"){
                                        $$('#currentSong-source').html('<i class="fa fa-youtube-play fa-2x" style="color:#bb0000"></i><br/><small>Source</small>');
                                    }else{
                                        $$('#currentSong-source').html('<i class="fa fa-soundcloud fa-2x" style="color:#ff3a00"></i><br/><small>Source</small>');
                                    }
                                    currentLink=resp.data[0].link;
                                    $$('#currentSong-source').data('url',resp.data[0].link);
                                }
                            }else{
                                $$('#screenshot').hide();
                                $$('#no-song').show();
                                countRequest=0;
                                tooFar=false;
                                lastId=0;
                            }
                            //antrian
                            var html="";
                            for(var i=1;i<resp.data.length;i++){
                                var image = assetUrl+"images/user.jpg";
                                var medal="";
                                if(isValidURL(resp.data[i].twitter_image)!=true){
                                    image=assetUrl+resp.data[i].twitter_image;
                                }else{
                                    image=resp.data[i].twitter_image;
                                }
                                var classItem="";
                                lastId=resp.data[i].user_id;
                                if(resp.data[i].user_id==mostRequest[0]){
                                    classItem="firstRank";
                                    medal="<img src='img/gold.png' width='20'/>";
                                }else if(resp.data[i].user_id==mostRequest[1]){
                                    classItem="secondRank";
                                    medal="<img src='img/silver.png' width='20'/>";
                                }else if(resp.data[i].user_id==mostRequest[2]){
                                    classItem="thirdRank";
                                    medal="<img src='img/bronze.png' width='20'/>";
                                }
                                var deleteOption="";
                                if(resp.data[i].user_id==userid) {
                                    countRequest++;
                                    deleteOption='<i class="fa fa-close delete-song" data-playlistid="'+resp.data[i].playlist_id+'" style="color: black; font-size: 18px;text-align: center; vertical-align: middle; "></i>';
                                }else{
                                    deleteOption='&nbsp;';
                                }
                                html+='<li class="item-content" style="border-bottom:1px solid #d2d2d2" >'+
                                    '<div class="item-media" >'+
                                    '<a href="#profile"><img class="'+classItem+'" src="'+image+'" style="width: 50px;border-radius: 25px" data-userid="'+resp.data[i].user_id+'" data-username="'+resp.data[i].createby+'"/></a>'+
                                    '</div>'+
                                    '<div class="item-inner" style="font-size: 12px;">'+
                                    '<table border="0" width="100%" cellspacing="0" cellpadding="1">'+
                                    '<tr>'+
                                    '<td width="85%">'+
                                    '<table border="0" width="100%" cellspacing="0" cellpadding="1">'+
                                    '<tr>'+
                                    '<td>'+medal+' <b>'+resp.data[i].createby+'</b></td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td>'+resp.data[i].title+'</td>'+
                                    '</tr>'+
                                    '<tr>'+
                                    '<td>'+resp.data[i].caption+'</td>'+
                                    '</tr>'+
                                    '</table>'+
                                    '</td>'+
                                    '<td width="15%" align="center" valign="center">'+deleteOption+
                                    '</td>'+
                                    '</tr>'+
                                    '</table>'+
                                    '</div>'+
                                    '</li>';
                            }
                            $$('#antrian').html(html);
                        }else{

                        }
                        $$('#loader').hide();
                    });
                    socket.on("mostRequest",function(data) {
                        var result = JSON.parse(data);
                        var i=0;
                        var html="";
                        var nomor=1;
                        $$.each(result, function (k,v) {
                            var classItem;
                            var medal="";
                            var imageUser = assetUrl+"images/user.jpg";
                            if(isValidURL(v.twitter_image)!=true){
                                imageUser=assetUrl+v.twitter_image;
                            }else{
                                imageUser=v.twitter_image;
                            }
                            if(i==0){
                                classItem="firstRank";
                                medal="<img src='img/gold.png' width='20'/>";
                            }else if(i==1){
                                classItem="secondRank";
                                medal="<img src='img/silver.png' width='20'/>";
                            }else if(i==2){
                                classItem="thirdRank";
                                medal="<img src='img/bronze.png' width='20'/>";
                            }else{
                                medal="<div style='width: 20px'></div>";
                            }
                            if(nomor%2 == 0)
                            {
                                html += "<li class='item-content' style='border-bottom:1px solid #d2d2d2;background-color:#f5f5f5'>";
                            }
                            else
                            {
                                html += "<li class='item-content' style='border-bottom:1px solid #d2d2d2'>";
                            }

                            html += "<div class='item-media'>"+medal+"<a href='#profile'><img style='width: 40px; border-radius: 25px' src='"+imageUser+"' class='"+classItem+"' data-userid='"+v.user_id+"' data-username='"+v.username+"'/></a></div>";
                            html += "<div class='item-inner'><div class='item-title'>"+v.username+"</div>";
                            html += "<div class='item-after' style='max-height:88px'><div class='chip'><div class='chip-label'><b>"+v.counter+" song</b></div></div></div>";
                            // html += "<div class='item-after'><b>"+v.counter+"</b></div>";
                            html += "</div></li>";
                            nomor++;
                            mostRequest[i]=v.user_id;i++;
                        });
                        $$('#topusers').html(html);
                    });
                    socket.on("whosLogin",function(data) {
                        var result = JSON.parse(data);
                        var html = "";
                        var nomor = 1;
                        for(var i=result.length-1;i>=0;i--) {
                            var split = result[i].split('•♪•');
                            var rank = "";
                            var medallink = "";

                            if(nomor%2 == 0){
                                html += "<li class='item-content' style='border-bottom:1px solid #d2d2d2;background-color:#f5f5f5'>";
                            }else{
                                html += "<li class='item-content' style='border-bottom:1px solid #d2d2d2'>";
                            }


                            // html += "<div class='item-media' style='min-width:30px'></div>";
                            html += "<div class='item-inner'><div class='item-title' style='float:left'>"+nomor+". <a href='#profile' style='color:#000;'  data-userid='"+split[1]+"' data-username='"+split[0]+"'>"+split[0]+"</a></div>";
                            if(result[i].match("admin"))
                            {
                                html += "<div class='item-after'><i class='fa fa-laptop fa-2x'></i></div>";
                            }
                            else
                            {
                                html += "<div class='item-after'><i class='fa fa-user fa-2x'></i></div>";
                            }
                            html += "</div></li>";
                            nomor++;
                        }
                        $$("#loginFriends").html(html);
                    });
                    socket.on("recentSong",function(data) {
                        var result = JSON.parse(data);
                        var html="";
                        var nomorBG = 1;
                        $$.each(result, function (k,v) {
                            var from="";
                            if(v.link.match(/youtube/)){
                                var icon = "<i class='fa fa fa-youtube-play fa-2x' style='color:#bb0000'></i>";
                                var from="youtube";
                            }else{
                                var icon = "<i class='fa fa-soundcloud fa-2x' style='color:#ff3a00'></i>";
                                var from="soundcloud";
                            }
                            if(nomorBG % 2 == 0)
                            {
                                html += "<li class='item-content' data-title='"+v.title+"' data-songid='"+v.track_id+"' data-from='"+from+"' data-loc='' data-genre='"+v.genre+"' data-link='"+v.link+"' style='background-color:#f5f5f5'>";
                            }
                            else
                            {
                                html += "<li class='item-content' data-title='"+v.title+"' data-songid='"+v.track_id+"' data-from='"+from+"' data-loc='' data-genre='"+v.genre+"' data-link='"+v.link+"' >";
                            }

                            html += "<div class='item-media' style='min-width:30px;max-width:30px'>"+icon+"</div>";
                            html += "<div class='item-inner'><div class='item-title'>"+v.title+"</div>";
                            html += "<div class='item-after' style='text-align:center'><i class='fa fa-play-circle fa-2x'></i></div>";
                            html += "</div></li>";
                            nomorBG++;
                        });
                        $$('#recentSongs').html(html);
                    });
                    socket.on("mostPlayed",function(data) {
                        var result = JSON.parse(data);
                        var html="";
                        var nomorBG = 1;
                        $$.each(result, function (k,v) {
                            var from="";
                            if(v.link.match(/youtube/)){
                                var icon = "<i class='fa fa-youtube-play fa-2x' style='color:#bb0000'></i>";
                                var from="youtube";
                            }else{
                                var icon = "<i class='fa fa-soundcloud fa-2x' style='color:#ff3a00'></i>";
                                var from="soundcloud";
                            }
                            if(nomorBG % 2 == 0)
                            {
                                html += "<li class='item-content' data-title='"+v.title+"' data-songid='"+v.track_id+"' data-from='"+from+"' data-loc='' data-genre='"+v.genre+"' data-link='"+v.link+"' style='background-color:#f5f5f5'>";
                            }
                            else
                            {
                                html += "<li class='item-content' data-title='"+v.title+"' data-songid='"+v.track_id+"' data-from='"+from+"' data-loc='' data-genre='"+v.genre+"' data-link='"+v.link+"' >";
                            }

                            html += "<div class='item-media' style='min-width:30px;max-width:30px'>"+icon+"</div>";
                            html += "<div class='item-inner'><div class='item-title'>"+nomorBG+". "+v.title+"</div>";
                            html += "<div class='item-after' style='text-align:center'><i class='fa fa-play-circle fa-2x'></i></div>";
                            html += "</div></li>";
                            nomorBG++;
                        });
                        $$('#weeklyChart').html(html);
                    });
                    socket.on('message2', function(data) {
                        var result = JSON.parse(data);
                        // console.log(result);
                        var chat = "";
                        $$.each(result, function (x, y) {
                            chat+=chatInsert(y);
                        });
                        $$('#messageBox').append(chat);
                        if(isChat){
                            messageCount=0;
                            initCount();
                        }else{
                            messageCount++;
                            initCount();
                        }
                        $('#tab_chat').animate({scrollTop: $('#messageBox').height() });
                    });
                    socket.on("getAllMessage", function(data) {
                        var result = JSON.parse(data);
                        var chat = "";
                        // console.log(data);
                        $$.each(result, function (x, y) {
                            if (y != "flagFirstTime") {
                                chat+=chatInsert(y);
                            }
                        });
                        $$("#messageBox").html(chat);
                        $('#tab_chat').animate({scrollTop: $('#messageBox').height() });
                    });
                }catch (err){
                    myApp.confirm("Can't Connect To Server. Reload ?","Jukebox 5D",function () {
                        location.reload();
                    },function () {
                        navigator.app.exitApp();
                    });
                }
                $$('#logout').on('click', function (e) {
                    storage.setItem("login","false");
                    storage.setItem("location","");
                    location.href="login.html";
                });
                $$('#privacy').on('click', function (e) {
                    location.href="privacy.html";
                });
                $$('#reply-delete').on('click', function (e){
                    $$('#reply').hide();
                    $$('#reply-text').html('');
                });
                $$('#about').on('click', function (e) {
                    location.href="about.html";
                });
                $$('#user-playlist').on('click', function (e) {
                    location.href="user_playlist.html";
                });
                $$('#user-playlist-popup').on('click', function (e) {
                    location.href="user_playlist.html";
                });
                $$('#currentSong-source').on('click',function (e) {
                    var url = $$(this).data("url");
                    cordova.InAppBrowser.open(url,'_blank', 'location=yes');
                });
                $$('#currentSong-img').on('click',function (e) {
                    var user_id=$$(this).data('userid');
                    var username=$$(this).data('username');
                    showProfile(user_id,username);
                });
                $$('#btnRecent').on('click',function (e) {
                    $$(this).addClass('active');
                    $$('#tabRecent').addClass('active');
                    $$('#btnWeekly').removeClass('active');
                    $$('#tabWeekly').removeClass('active');
                });
                $$('#btnWeekly').on('click',function (e) {
                    $$(this).addClass('active');
                    $$('#tabWeekly').addClass('active');
                    $$('#btnRecent').removeClass('active');
                    $$('#tabRecent').removeClass('active');
                });
                $$('#choose').on('click',function (e) {
                    storage.setItem('location','');
                    storage.setItem('location_id','');
                    location.href='choose.html';
                });
                $$('#edit-profile').on('click',function (e) {
                    location.href='edit.html';
                });
                $$('#howto').on('click',function (e) {
                    cordova.InAppBrowser.open('https://www.youtube.com/embed/_MTlNfKSNlQ', '_blank', 'location=yes');
                });
                function jukeDialog(data,reload) {
                    if(countRequest>=2 || lastId==userid || tooFar){
                        if(countRequest>=2 || lastId==userid){
                            myApp.alert("It's not your turn yet. Invite your friend to insert a song first. (max. 2 songs per user)",'Jukebox5D');

                        }else{
                            myApp.alert("Too Far From Admin",'Jukebox5D');
                        }
                    }else{
                        // navigator.notification.confirm(
                        //     "Yuhu, lagu "+data.title+" sudah terpilih, mau di apakan lagunya?", // the message
                        //     function( index ) {
                        //         switch ( index ) {
                        //             case 1:
                        //                myApp.confirm('Add this song to playlist? <br/> '+data.title,'Jukebox 5D',
                        //                     function () {
                        //                         myApp.prompt('Tell us how you feel about this song!','Jukebox 5D', function (value) {
                        //                             data.cap=encodeURIComponent(value);
                        //                             socket.emit("insertSong",data);
                        //                             myApp.alert('Wait until it is played.','Jukebox5D',function () {
                        //                                 if(reload){
                        //                                     location.reload();
                        //                                 }
                        //                             });
                        //                         });
                        //                         lastId=userid;
                        //                         countRequest++;
                        //                     },
                        //                     function () {

                        //                     }
                        //                 );
                        //                 break;
                        //             case 2:
                        //                 myApp.popup('.popup-library');
                        //                 break;
                        //             case 3:
                        //                 // The third button was pressed
                        //                 break;
                        //         }
                        //     },
                        //     "Jukebox5D",                   // a title
                        //     [ "Mainkan Lagu", "Tambah ke Library", "Batal" ] // text of the buttons
                        // );

                        myApp.confirm('Add this song to playlist? <br/> '+data.title,'Jukebox 5D',
                                            function () {
                                                myApp.prompt('Tell us how you feel about this song!','Jukebox 5D', function (value) {
                                                    data.cap=encodeURIComponent(value);
                                                    socket.emit("insertSong",data);
                                                    myApp.alert('Wait until it is played.','Jukebox5D',function () {
                                                        if(reload){
                                                            location.reload();
                                                        }
                                                    });
                                                });
                                                lastId=userid;
                                                countRequest++;
                                            },
                                            function () {

                                            }
                                        );


                    }
                }
                function deleteSong(playlist_id) {
                    myApp.confirm('Delete Your Song?','Jukebox 5D',
                        function () {
                            var data={};
                            data.playlist_id=playlist_id;
                            data.loc=varLoc;
                            socket.emit("deleteMySong", data);
                            lastId=0;
                        },
                        function () {

                        });
                }
                function skipLove(option){
                    skipAble=false;
                    socket.emit("likedislike",{playlist_id: currentPlaylistId, userid: userid, option: option, loc: varLoc});
                }
                $$(document).on('click', '.to-reply', function (e) {
                    userReply=$$(this).data('replyuser');
                    $$('#reply').show();
                    $$('#reply-text').html($$(this).data('reply'));
                });
                $$(document).on('click', '#antrian li img', function (e) {
                    var user_id=$$(this).data('userid');
                    var username=$$(this).data('username');
                    showProfile(user_id,username);
                });
                $$(document).on('click', '#topusers li img', function (e) {
                    var user_id=$$(this).data('userid');
                    var username=$$(this).data('username');
                    showProfile(user_id,username);
                });
                $$(document).on('click', '#loginFriends li a', function (e) {
                    var user_id=$$(this).data('userid');
                    var username=$$(this).data('username');
                    showProfile(user_id,username);
                });
                $$(document).on('click', '#weeklyChart li', function (e) {
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.loc=varLoc;
                    data.from=$$(this).data('from');
                    data.link=$$(this).data('link');
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=1;
                    jukeDialog(data,false);
                });
                $$(document).on('click', '#recentSongs li', function (e) {
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.loc=varLoc;
                    data.from=$$(this).data('from');
                    data.link=$$(this).data('link');
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=0;
                    jukeDialog(data,false);
                });
                $$(document).on('click', '#recentsProfile li', function (e) {
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.loc=varLoc;
                    data.from=$$(this).data('from');
                    data.link=$$(this).data('link');
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=0;
                    jukeDialog(data,false);
                });
                $$(document).on('click', '.delete-song', function (e) {
                    var playlist_id=$$(this).data('playlistid');
                    deleteSong(playlist_id);
                });
                $$(document).on('click','#currentSong-delete',function (e) {
                    deleteSong(currentPlaylistId);
                });
                $$('#currentSong-like').on('click',function (e) {
                    if(skipAble){
                        skipLove('like');
                    }
                });
                $$('#currentSong-dislike').on('click',function (e) {
                    if(skipAble) {
                        skipLove('dislike');
                    }
                });

                getPlaylist();

                $$('#addToLibrary').on('click',function(e){
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.loc=varLoc;
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=1;

                    data.track_id=$('#addToLibrary').data('track_id');
                    data.title=$('#addToLibrary').data('title');
                    data.link=$('#addToLibrary').data('link');

                    document.getElementById('id-library').setAttribute('data-title', data.title);
                    document.getElementById('id-library').setAttribute('data-link', data.link);
                    document.getElementById('id-library').setAttribute('data-songid', data.track_id);
                    getPlaylist();
                     myApp.popup('.popup-library');
                     getPlaylist();
                     
                 });
                
                $$('#currentSong-share').on('click',function(e){
                    myApp.modal({
                        title:  'Jukebox5D',
                        text: 'Choose Sharing Type : ',
                        buttons: [
                            {
                                text: 'Text',
                                onClick: function() {
                                    var DefaultCaption=[
                                        "Music is to the soul what words are to the mind. (M.M)",
                                        "Music is the moonlight in the gloomy night of life. (J.P)",
                                        "Music is the literature of the heart. (A.L)",
                                        "Without music, life would be a mistake. (F.N)",
                                        "Music is a second language to my heart. (M.A)",
                                        "Where words leave off, music begins. (H.H)",
                                        "Where words fail, music speaks. (H.A)",
                                        "Music is the universal language of mankind. (H.L)",
                                        "Music is the strongest form of magic. (M.M)",
                                        "Music is love in search of a word. (S.L)"
                                    ];
                                    if(currentCaption==""){
                                        var index=Math.floor((Math.random() * (DefaultCaption.length-1)) + 1);
                                        currentCaption=DefaultCaption[index];
                                    }
                                    window.plugins.socialsharing.share(
                                        currentCaption+'-'+currentUser+' \n #DengerBareng '+currentTitle+' di '+locationAdmin+' pake #Jukebox5D. \n  How About You ? '+currentLink,
                                        'Jukebox5d',
                                        null,
                                        'http://www.jukebox5d.com');
                                    currentCaption="";
                                }
                            },
                            {
                                text: 'Image',
                                onClick: function() {
                                    var DefaultCaption=[
                                        "Music is to the soul what words are to the mind. (M.M)",
                                        "Music is the moonlight in the gloomy night of life. (J.P)",
                                        "Music is the literature of the heart. (A.L)",
                                        "Without music, life would be a mistake. (F.N)",
                                        "Music is a second language to my heart. (M.A)",
                                        "Where words leave off, music begins. (H.H)",
                                        "Where words fail, music speaks. (H.A)",
                                        "Music is the universal language of mankind. (H.L)",
                                        "Music is the strongest form of magic. (M.M)",
                                        "Music is love in search of a word. (S.L)"
                                    ];
                                    if(currentCaption==""){
                                        var index=Math.floor((Math.random() * (DefaultCaption.length-1)) + 1);
                                        currentCaption=DefaultCaption[index];
                                    }
                                    var textShare=currentCaption+'-'+currentUser+' \n #DengerBareng '+currentTitle+' di '+locationAdmin+' pake #Jukebox5D. \n How about you ? '+currentLink;
                                    $$.post(image_api, {
                                        text: currentCaption+' \n '+currentUser+' #DengerBareng di '+locationAdmin
                                    }, function (response) {
                                        window.plugins.socialsharing.share(
                                            textShare,
                                            'Jukebox5d',
                                            response,
                                            'http://www.jukebox5d.com');
                                        currentCaption="";
                                    }, function (error) {
                                        alert("Network Error");
                                    });
                                }
                            }
                        ]
                    });
                });
                $$('.tab-index').on('show', function () {
                    $$('#fab').show();

                    $$('#btn-index').prop('src','img/jukebox_icon_playing_white.png');
                    $$('#page-title').html('Playlist');
                    myApp.showTab('.tab-index');
                });
                $$('.tab-chat').on('show', function () {
                    $$('.messagebar').show();
                    $$('#btn-chat').prop('src','img/jukebox_icon_chat_white.png');
                    $$('#page-title').html('Chat Lounge');
                    isChat=true;
                    messageCount=0;
                    initCount();
                    myApp.showTab('.tab-chat');
                });
                $$('.tab-recents').on('show', function () {
                    $$('#btn-recents').prop('src','img/jukebox_icon_recent_white.png');
                    $$('#page-title').html('Music Trends');
                    myApp.showTab('.tab-recents');
                });
                $$('.tab-topusers').on('show', function () {
                    $$('#btn-topusers').prop('src','img/jukebox_icon_chart_white.png');
                    $$('#page-title').html('Top Users');
                    myApp.showTab('.tab-topusers');
                });
                $$('.tab-whoslogin').on('show', function () {
                    $$('#btn-whoslogin').prop('src','img/jukebox_icon_login_white.png');
                    $$('#page-title').html('Online Users');
                    myApp.showTab('.tab-whoslogin');
                });
                $$('.tab-index').on('hide', function () {
                    $$('#fab').hide();
                    $$('#btn-index').prop('src','img/jukebox_icon_playing_gray.png');
                });
                $$('.tab-whoslogin').on('hide', function () {
                    $$('#btn-whoslogin').prop('src','img/jukebox_icon_login_gray.png');
                });
                $$('.tab-chat').on('hide', function () {
                    $$('.messagebar').hide();
                    $$('#btn-chat').prop('src','img/jukebox_icon_chat_gray.png');
                    isChat=false;
                });
                $$('.tab-recents').on('hide', function () {
                    $$('#btn-recents').prop('src','img/jukebox_icon_recent_gray.png');
                });
                $$('.tab-topusers').on('hide', function () {
                    $$('#btn-topusers').prop('src','img/jukebox_icon_chart_gray.png');
                });
                $$('#follow').on('click',function (e) {
                    var params={};
                    params.user_id=currentUserIDProfile;
                    params.token= storage.getItem("token");
                    $$.post(follow_url,params, function (data) {
                        showProfile(currentUserIDProfile,currentUsernameProfile);
                    });
                });
                $$('#location-btn').on('click',function (e) {
                    location.href="choose.html";
                });
                $$('#sendMessage').on('click',function (e) {
                    var reply=$$('#reply-text').html();
                    var data={};
                    var str=$$('#textMessage').val();
                    if(reply!=""){
                        str=userReply+"•♪•"+reply+"•♪•"+str;
                    }
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();
                    var hours = today.getHours();
                    var minutes = today.getMinutes();
                    var seconds = today.getSeconds();
                    if(dd<10) {
                        dd='0'+dd;
                    }
                    if(mm<10) {
                        mm='0'+mm;
                    }
                    var regex = /(<([^>]+)>)/ig
                    today = yyyy+'/'+mm+'/'+dd+' '+hours+':'+minutes+':'+seconds;
                    data.username = username;
                    data.userid = userid;
                    data.chat = Base64.encode(str.replace(/'/g, "\\'").replace(regex, ""));
                    data.image = myImage;
                    data.time = today;
                    data.loc = varLoc;
                    socket.emit("message2",data);
                    $$('#textMessage').val('');
                    $$('#reply').hide();
                });
                $$('#unfollow').on('click',function (e) {
                    var params={};
                    params.user_id=currentUserIDProfile;
                    params.token= storage.getItem("token");
                    $$.post(unfollow_url,params, function (data) {
                        showProfile(currentUserIDProfile,currentUsernameProfile);
                    });
                });


                function getPlaylist() {
                    var params={};
                    params.token=storage.getItem('token');
                 

                    $$.get(get_playlist,params, function (data) {
                        var response=JSON.parse(data);
                        if(response.status){
                            var html="";

                             

                            for (var i=0;i<response.data.length;i++){

                                   //  html+=' <li> <a href="#"  data-playlistname="'+response.data[i].playlist_name+'" data-playlistid="'+response.data[i].playlist_id+'" class="item-link item-content">' +
                                   // '<div class="item-inner">'+
                                   // '<div class="item-title">'+response.data[i].playlist_name+'</div>'+
                                   // '</div></a>'+
                                   // '</li>nan';

                                   $("#library-id > option").ready(function(){
                                        html+= '<option value = "'+response.data[i].playlist_id+'"> '+response.data[i].playlist_name+' </option>';
                                        // html += response.data[i].playlist_name;
                                    });

                            }
                          $$('#library-id').html(html);
                        }
                    },function (error) {
                        alert("Network Error");
                    });
                }

                $$(document).on('click', '.button-library', function () {
                    // getPlaylist();
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.track_id=$('.button-library').data('songid');
                    data.title=$('.button-library').data('title');
                    data.link=$('.button-library').data('link');
                    data.playlist_id=$('#library-id option:selected').val();
                    alert(""+data.playlist_id+"|"+data.track_id +"-"+data.title);


                     selectDB(data);


                    // alert("this is id : "+data.id+"this is link : "+data.link+"this is title: "+data.title + "this is playlist id :" +playlist_id);



                    // selectDB(data.id,data.link,data.title);
                        
                });


                function selectDB(data){
                        data.token=storage.getItem('token');
                        window.plugins.spinnerDialog.show(null, "Loading ...", true);
                        $$.post(add_item_playlist, data, function (response) {
                        window.plugins.spinnerDialog.hide();
                        var json=JSON.parse(response);
                        if(json.status){
                            myApp.alert(json.message,"Jukebox5D");
                        }else{
                            myApp.alert(json.message,"Jukebox5D");
                        }
                            document.getElementById("tombol-tutup-popup").click();

                    }, function (error) {
                        alert("Network Error");
                        window.plugins.spinnerDialog.hide();
                    });
                }


                function onSuccess(position) {
                    var distance=showDistanceFromServer(position.coords.latitude,position.coords.longitude);
                    if(distance>1000){
                        tooFar=true;
                    }else{
                        tooFar=false;
                    }
                }
                function showDistanceFromServer(latitude, longitude) {
                    var lat2 = lat;
                    var lon2 = long;
                    var R = 6378137; // Radius of the earth in m
                    var dLat = deg2rad(lat2-latitude);  // deg2rad below
                    var dLon = deg2rad(lon2-longitude);
                    var a =
                            Math.sin(dLat/2) * Math.sin(dLat/2) +
                            Math.cos(deg2rad(latitude)) * Math.cos(deg2rad(lat2)) *
                            Math.sin(dLon/2) * Math.sin(dLon/2)
                        ;
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    var d = R * c; // Distance in m
                    console.log(d);
                    return d.toFixed(1);
                }
                function deg2rad(deg) {
                    return deg * (Math.PI/180)
                }

                function onError(error) {
                    console.log('code: '    + error.code    + '\n' +
                        'message: ' + error.message + '\n');
                }
                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationAvailable(function (available) {
                        if (available) {
                        navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });
                        } else {
                            myApp.alert("Turn On GPS And Restart App !", 'Jukebox5D',function () {
                                cordova.plugins.diagnostic.switchToLocationSettings();

                            });
                        }
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }
            }else{
                location.href="choose.html";
            }
        }else{
            location.href="login.html";
        }
    }
};

app.initialize();

/**
 * Created by mrkai303 on 01/12/16.
 */
/**
 * Created by mrkai303 on 01/12/16.
 */
window.onerror = function(message, url, lineNumber) {
    //alert("Error: "+message+" in "+url+" at line "+lineNumber);
}
var app = {
    initialize: function() {
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        var locationStatus=false;
        var locationAdmin = storage.getItem("location");
        if(isLogin=='true') {
            var myApp = new Framework7();
            var $$ = Dom7;
            var mainView = myApp.addView('.view-main', {domCache: true});
            $$('.pull-to-refresh-content').on('refresh', function (e) {
                getLocation();
                myApp.pullToRefreshDone();
            });
            var onSuccess = function(position) {
                locationStatus=true;
                storage.setItem("latlong",position.coords.latitude+"#"+position.coords.longitude);
                var params={
                    lat : position.coords.latitude,
                    long : position.coords.longitude,
                    token : storage.getItem("token")
                };
                window.plugins.spinnerDialog.show(null,"Loading ...", true);
                $$.post(choose_url,params, function (data) {
                    var response=JSON.parse(data);
                    if(response.status){
                        var html="";
                        // alert(JSON.stringify(response.data));
                        window.plugins.spinnerDialog.hide();
                        if(response.data.length<1){
                            myApp.alert('No Jukebox Admin Around You. Pull to Refresh !','Jukebox5D');
                        }
                        // $$.each(response.data,function (key,value) {
                        // html+='<li data-latlong="'+value.latlong+'"  data-userid="'+value.user_id+'" data-location="'+value.location+'">'+
                        // '<div class="location-item item-content">'+
                        // '<div class="item-media">'+
                        // '<i class="fa fa-map-marker color-cyan"></i>'+
                        // '</div>'+
                        // '<div class="item-inner">'+
                        // value.location+'('+value.distance+' m)'+
                        // '</div>'+
                        // '</div>'+
                        // '</li>';
                        // });
                        $$.each(response.data,function (key,value) {
                            html+='<div data-latlong="'+value.latlong+'"  data-userid="'+value.user_id+'" data-location="'+value.location+'" class="chip">'+
                                '<div class="chip-media bg-blue"><i class="fa fa-map-marker"></i></div>'+
                                '<div class="chip-label">'+
                                value.location+' ('+value.distance+' m)'+
                                '</div>'+
                                '</div>';
                        });
                        $$('#location-list').html(html);
                    }else{
                        window.plugins.spinnerDialog.hide();
                        myApp.confirm(response.message+'. Reconnect ?','Jukebox 5D',
                            function () {
                                getLocation();
                            },
                            function () {

                            }
                        );
                    }
                },function (error) {
                    myApp.alert("Network Error",'Jukebox5D');
                });
                // window.plugins.spinnerDialog.hide();
            };
            var onError=function (error) {
                myApp.alert("Failed to get Location.\nPlease turn on location permission and gps !",'Jukebox5D');
            }
            function getLocation() {
                navigator.geolocation.getCurrentPosition(onSuccess, onError);
            }
            $$('#logout').on('click', function (e) {
                storage.setItem("login","false");
                storage.setItem("location","");
                location.href="login.html";
            });
            $$(document).on('click', '#refresh',function (e) {
                getLocation();
            });
            $$(document).on('click', '#location-list .chip', function (e) {
                storage.setItem("location",$$(this).data('location'));
                storage.setItem("location_id",$$(this).data('userid'));
                storage.setItem("latlong",$$(this).data('latlong'));
                location.href="index.html";
            });
            document.addEventListener('deviceready', function () {
                getLocation();
                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationAvailable(function (available) {
                        if (available==true) {
                            getLocation();
                        } else {
                            myApp.alert("Turn On GPS And Restart App !", 'Jukebox5D',function () {
                                cordova.plugins.diagnostic.switchToLocationSettings();
                            });
                        }
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }
                /*
                codePush.sync(function (status) {
                    switch (status) {
                        case SyncStatus.DOWNLOADING_PACKAGE:
                            window.plugins.spinnerDialog.show(null, "Downloading Updates...", true);
                            break;
                        case SyncStatus.INSTALLING_UPDATE:
                            window.plugins.spinnerDialog.hide();
                            window.plugins.spinnerDialog.show(null, "Installing Updates...", true);
                            break;
                        case SyncStatus.UPDATE_INSTALLED:
                            window.plugins.spinnerDialog.hide();
                            break;
                    }
                },{ updateDialog: true });
                 */
            }, false);
        }else{
            location.href="login.html";
        }
    },
    onDeviceReady: function() {

    }
};

app.initialize();

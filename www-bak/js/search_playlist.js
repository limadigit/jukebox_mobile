/**
 * Created by Indocyber on 19/08/2017.
 */
window.onerror = function(message, url, lineNumber) {
    alert("Error: " + message + " in " + url + " at line " + lineNumber);
}
$('.tabs').show();
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        if(isLogin=='true') {
            if(locationAdmin!='' && locationAdmin!=null){
                var myApp = new Framework7();
                var $$ = Dom7;
                var userid=storage.getItem('user_id');
                var playlist_id=storage.getItem('playlist_id');
                var username=storage.getItem('username');
                var email=storage.getItem('email');
                var myImage=storage.getItem('image');
                var varLoc=storage.getItem('location_id');
                var latlongTemp=storage.getItem('latlong').split('#');
                var lat=latlongTemp[0];
                var long=latlongTemp[1];
                var mainView = myApp.addView('.view-main', {domCache: true})
                var mySearchbar = myApp.searchbar('.searchbar', {
                    searchList: '.list-block-search',
                    searchIn: '.item-title'
                });
                $$('#back-btn').on('click',function (e) {
                    history.back();
                });
                $$('#search-input').on('change',function (e) {
                    var params={};
                    params.token=storage.getItem('token');
                    params.loc=storage.getItem('location_id');
                    params.q=mySearchbar.query;
                    window.plugins.spinnerDialog.show(null,"Loading ...", true);
                    $$.get(search_url+'?q='+mySearchbar.query+'&loc='+storage.getItem('location_id'),params, function (data) {
                        window.plugins.spinnerDialog.hide();
                        var response=JSON.parse(data);
                        if(response.status){
                            var html="";
                            $$('.searchbar-not-found').hide();
                            $$('.searchbar-found').show();
                            for(var i=0;i<response['length'];i++){
                                var from="";
                                if(response[i].link.match(/youtube/)){
                                    var icon = "<i class='fa fa fa-youtube-play fa-2x' style='color:#bb0000'></i>";
                                    from="youtube";
                                }else{
                                    var icon = "<i class='fa fa-soundcloud fa-2x' style='color:#ff3a00'></i>";
                                    from="soundcloud";
                                }
                                if(i%2==0){
                                    html += "<li class='item-content' data-title='"+response[i].title+"' data-songid='"+response[i].track_id+"' data-from='"+from+"' data-loc='' data-genre='"+response[i].genre+"' data-link='"+response[i].link+"' style='border-bottom:1px solid #d2d2d2;background-color:#f5f5f5'>";
                                }else{
                                    html += "<li class='item-content' data-title='"+response[i].title+"' data-songid='"+response[i].track_id+"' data-from='"+from+"' data-loc='' data-genre='"+response[i].genre+"' data-link='"+response[i].link+"' style='border-bottom:1px solid #d2d2d2'>";
                                }

                                html += "<div class='item-media' style='min-width:30px;max-width:30px'>"+icon+"</div>";
                                html += "<div class='item-inner'><div class='item-title'>"+response[i].title+"</div>";
                                html += "<div class='item-after' style='text-align:center'><i class='fa fa-plus-circle" +
                                    " fa-2x' style='color:#00aeff'></i></div>";
                                html += "</div></li>";
                            }
                            $$('#searchResult').html(html);
                        }else{
                            $$('.searchbar-not-found').html(response.errMobile)
                            $$('.searchbar-not-found').show();
                            $$('.searchbar-found').hide();
                        }
                    },function (error) {
                        window.plugins.spinnerDialog.hide();
                        alert("Network Error");
                    });
                });
                $$(document).on('click', '#searchResult li', function (e) {
                    var data={};
                    data.track_id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.link=$$(this).data('link');
                    data.playlist_id=playlist_id;
                    insertSong(data);
                });
                function insertSong(data) {
                    data.token=storage.getItem('token');
                    window.plugins.spinnerDialog.show(null, "Loading ...", true);
                    $$.post(add_item_playlist, data, function (response) {
                        window.plugins.spinnerDialog.hide();
                        var json=JSON.parse(response);
                        if(json.status){
                            myApp.alert(json.message,"Jukebox5D");
                        }else{
                            myApp.alert(json.message,"Jukebox5D");
                        }
                    }, function (error) {
                        alert("Network Error");
                        window.plugins.spinnerDialog.hide();
                    });
                }
            }else{
                location.href="choose.html";
            }
        }else{
            location.href="login.html";
        }
    }
};

app.initialize();

/**
 * Created by mrkai303 on 20/12/16.
 */
var myApp = new Framework7();
var $$ = Dom7;
var mainView = myApp.addView('.view-main', {domCache: true});
var options = {
    success:   function (responseText, statusText, xhr, $form) {
        var json=JSON.parse(responseText);
        if(json.status){
            myApp.alert("Check Your Email for Confirmation",'Jukebox5D');
            window.plugins.spinnerDialog.hide();
            setTimeout(function () {
                location.href='login.html';
            },3000);
        }else{
            myApp.alert(json.message.replace('/\n','<br>/'),'Jukebox5D');
            window.plugins.spinnerDialog.hide();
        }
    },
    beforeSubmit : function(arr, $form, options){
        if($$('#passwordRegister').val()!=$$('#cpasswordRegister').val()){
            return false;
        }else{
            window.plugins.spinnerDialog.show(null,"Loading ...", true);
            return true;
        }
    }
};
$('#form-register').ajaxForm(options);
document.addEventListener("backbutton", onBackKeyDown, false);
function onBackKeyDown() {
    location.href='login.html';
}
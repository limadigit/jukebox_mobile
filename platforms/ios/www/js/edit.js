/**
 * Created by mrkai303 on 01/12/16.
 */
/**
 * Created by mrkai303 on 01/12/16.
 */
window.onerror = function(message, url, lineNumber) {
    //alert("Error: "+message+" in "+url+" at line "+lineNumber);
}
var app = {
    initialize: function() {
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        var file=null;
        if(isLogin=='true') {
            var assetsUrl= 'https://jukebox5d.com/app/assets/';
            var userid=storage.getItem('user_id');
            var username=storage.getItem('username');
            var email=storage.getItem('email');
            var myImage=storage.getItem('image');
            var bio=storage.getItem('bio');
            var food=JSON.parse(storage.getItem("food"));
            var token=storage.getItem("token");
            var music_genre=JSON.parse(storage.getItem("music_genre"));
            var hangout_place=JSON.parse(storage.getItem("hangout_place"));
            var drink=JSON.parse(storage.getItem("drink"));
            var myApp = new Framework7();
            var $$ = Dom7;
            var mainView = myApp.addView('.view-main', {domCache: true});
            function isValidURL(url){
                var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                if(RegExp.test(url)){
                    return true;
                }else{
                    return false;
                }
            }
            if(!isValidURL(myImage)){
                myImage=assetsUrl+myImage;
            }
            $$('#imgProfile').prop('src',myImage);
            $$('#usernameProfile').val(username);
            $$('#emailProfile').val(email);
            $$('#aboutProfile').val(bio);
            $$('#token').val(token);
            $$('#musicsProfile').html(bio);
            $$('#back').on('click',function (e) {
               location.href="index.html";
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $$('#imgProfile').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $$("#img").on('change',function(){
                readURL(this);
            });
            var params={
                token : storage.getItem("token")
            };
            $$.post(music_url,params, function (resp) {
                var json=JSON.parse(resp);
                var html="";
                $$.each(json.data,function (k1,v1) {
                    var selected=false;
                    $$.each(music_genre,function (k2,v2) {
                        if(v1==v2){
                            selected=true;
                        }else{
                            selected=false;
                        }
                    });
                    if(selected){
                        html+=`<option value="${v1}" selected>${v1}</option>`;
                    }else{
                        html+=`<option value="${v1}">${v1}</option>`;
                    }
                });
                $$('#musicsProfile').html(html);
            });
            console.log(food);
            $$('#musics').val(JSON.stringify(music_genre));
            if(food.length<1){
                $('#foodsProfile').tagEditor({ initialTags: [] });
            }else{
                $('#foodsProfile').tagEditor({ initialTags: food });
            }
            if(drink.length<1) {
                $('#drinksProfile').tagEditor({initialTags: [] });
            }else{
                $('#drinksProfile').tagEditor({initialTags: drink});
            }
            if(hangout_place.length<1) {
                $('#placesProfile').tagEditor({initialTags: []});
            }else{
                $('#placesProfile').tagEditor({initialTags: hangout_place});
            }
            $('#foods').val(JSON.stringify(food));
            $('#drinks').val(JSON.stringify(drink));
            $('#places').val(JSON.stringify(hangout_place));
            $$('form.ajax-submit').on('submitted', function (e) {
                var xhr = e.detail.xhr; // actual XHR object
                var data = e.detail.data; // Ajax response from action file
                console.log(data);
            });
            $$('#musicsProfile').on('change',function (e) {
                var musicsList=[];
               $$(this).find('option:checked').each(function(){
                   console.log($$(this).val());
                   musicsList.push($$(this).val());
               });
               $$('#musics').val(JSON.stringify(musicsList));
            });
            var options = {
                clearForm: false ,       // clear all form fields after successful submit
                resetForm: false ,        // reset the form after successful submit
                success:   function (responseText, statusText, xhr, $form) {
                    var json=JSON.parse(responseText);
                    if(json.status) {
                        storage.setItem("image", json.twitter_image)
                        storage.setItem("username", $$('#usernameProfile').val());
                        storage.setItem("email", $$('#emailProfile').val());
                        storage.setItem("bio", $$('#aboutProfile').val());
                        storage.setItem("food", $$('#foods').val());
                        storage.setItem("music_genre", $$('#musics').val());
                        storage.setItem("hangout_place", $$('#places').val());
                        storage.setItem("drink", $$('#drinks').val());
                        console.log(json);
                        window.plugins.spinnerDialog.hide();
                        if ($$('#npasswordProfile').val() == "") {
                            myApp.alert("Profile has been updated", 'Jukebox5D');
                            setTimeout(function () {
                                location.href='index.html';
                            },3000);
                        } else {
                            if ($$('#npasswordProfile').val() == $$('#cpasswordProfile').val()) {
                                var params = {};
                                params.password = $$('#npasswordProfile').val();
                                params.token = token;
                                $$.post(password_url, params, function (data) {
                                    var json = JSON.parse(data);
                                    if (json.status) {
                                        storage.setItem('login', 'false');
                                        storage.setItem("location", "");
                                        location.href = "login.html";
                                    } else {
                                        myApp.alert(json.message, 'Jukebox5D');
                                    }
                                });
                            } else {
                                myApp.alert('Password Not Matched', 'Jukebox5D');
                            }
                        }
                    }else{
                        myApp.alert(json.message, 'Jukebox5D');
                    }
                },
                beforeSubmit : function(arr, $form, options){
                    $('#foods').val(JSON.stringify($('#foodsProfile').tagEditor('getTags')[0].tags));
                    $('#drinks').val(JSON.stringify($('#drinksProfile').tagEditor('getTags')[0].tags));
                    $('#places').val(JSON.stringify($('#placesProfile').tagEditor('getTags')[0].tags));
                    window.plugins.spinnerDialog.show(null,"Loading ...", true);
                    return true;
                }
            };
            $('#form-edit').ajaxForm(options);
        }else{
            location.href="login.html";
        }
    },
    onDeviceReady: function() {

    }
};

app.initialize();
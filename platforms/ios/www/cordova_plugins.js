cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "code-push.AcquisitionManager",
        "file": "plugins/code-push/script/acquisition-sdk.js",
        "pluginId": "code-push",
        "merges": [
            "window"
        ]
    },
    {
        "id": "cordova-plugin-chrome-apps-common.events",
        "file": "plugins/cordova-plugin-chrome-apps-common/events.js",
        "pluginId": "cordova-plugin-chrome-apps-common",
        "clobbers": [
            "chrome.Event"
        ]
    },
    {
        "id": "cordova-plugin-chrome-apps-common.errors",
        "file": "plugins/cordova-plugin-chrome-apps-common/errors.js",
        "pluginId": "cordova-plugin-chrome-apps-common"
    },
    {
        "id": "cordova-plugin-chrome-apps-common.stubs",
        "file": "plugins/cordova-plugin-chrome-apps-common/stubs.js",
        "pluginId": "cordova-plugin-chrome-apps-common"
    },
    {
        "id": "cordova-plugin-chrome-apps-common.helpers",
        "file": "plugins/cordova-plugin-chrome-apps-common/helpers.js",
        "pluginId": "cordova-plugin-chrome-apps-common"
    },
    {
        "id": "cordova-plugin-chrome-apps-system-network.system.network",
        "file": "plugins/cordova-plugin-chrome-apps-system-network/system.network.js",
        "pluginId": "cordova-plugin-chrome-apps-system-network",
        "clobbers": [
            "chrome.system.network"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "code-push": "2.0.1-beta",
    "cordova-plugin-add-swift-support": "1.6.0",
    "cordova-plugin-chrome-apps-common": "1.0.7",
    "cordova-plugin-chrome-apps-system-network": "1.1.2"
};
// BOTTOM OF METADATA
});
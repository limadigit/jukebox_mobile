window.onerror = function(message, url, lineNumber) {
    alert("Error: " + message + " in " + url + " at line " + lineNumber);
}
$('.tabs').show();
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var currentUserIDProfile=0,currentUsernameProfile="";
        var messageCount=0;
        var lastDate="";
        var isChat=false;
        var countRequest=0;
        var tooFar=false;
        var skipAble=true;
        var lastId=0;
        var currentPlaylistId=0;
        var currentTitle=0;
        var currentUser="";
        var currentLink="";
        var userReply="";
        var currentCaption="";
        var storage = window.localStorage;
        var mostRequest= new Array();
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        var assetUrl='https://jukebox5d.com/app/assets/';
        if(isLogin=='true') {
            if(locationAdmin!='' && locationAdmin!=null){
                var myApp = new Framework7();
                var $$ = Dom7;
                $$('#location-name').html(locationAdmin);
                var userid=storage.getItem('user_id');
                var profile_userid=storage.getItem('profile_user_id');
                var username=storage.getItem('username');
                var profile_username=storage.getItem('profile_username');
                var email=storage.getItem('email');
                var myImage=storage.getItem('image');
                var varLoc=storage.getItem('location_id');
                var latlongTemp=storage.getItem('latlong').split('#');
                var lat=latlongTemp[0];
                var long=latlongTemp[1];
                var mainView = myApp.addView('.view-main', {domCache: true});
                function isValidURL(url){
                    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                    if(RegExp.test(url)){
                        return true;
                    }else{
                        return false;
                    }
                }
                function showProfile(user_id,username) {
                    window.plugins.spinnerDialog.show(null, "Loading ...", true);
                    $$('#userinfo').hide();
                    currentUserIDProfile=user_id;
                    currentUsernameProfile=username;
                    $$('#menu').hide();
                    $$('#profile-navbar').show();
                    $$('#title-profile').html(username+"'s profile");
                    var params={};
                    params.user_id=user_id;
                    params.loc=varLoc;
                    params.token= storage.getItem("token");
                    $$.post(profile_url,params, function (data) {
                        var respone=JSON.parse(data);
                        if(respone.status){
                            var data=respone.data;
                            var image = assetUrl+"images/user.jpg";
                            var medal="";
                            if(isValidURL(data.twitter_image)!=true){
                                image=assetUrl+data.twitter_image;
                            }else{
                                image=data.twitter_image;
                            }
                            $$('#profile-img').prop('src',image);
                            if(data.user_id==mostRequest[0]){
                                $$('#profile-img').addClass("firstRank");
                                medal="<img src='img/gold.png' width='20'/>";
                            }else if(data.user_id==mostRequest[1]){
                                $$('#profile-img').addClass("secondRank");
                                medal="<img src='img/silver.png' width='20'/>";
                            }else if(data.user_id==mostRequest[2]){
                                $$('#profile-img').addClass("thirdRank");
                                medal="<img src='img/bronze.png' width='20'/>";
                            }else{
                                $$('#profile-img').removeClass("firstRank");
                                $$('#profile-img').removeClass("secondRank");
                                $$('#profile-img').removeClass("thirdRank");
                            }
                            $$('#profile-username').html(medal+data.username);
                            $$('#profile-desc').html(data.bio);
                            $$('#profile-fol').html(data.following+' following | '+data.followers+' followers');
                            if(data.is_follow || data.user_id==userid){
                                $$('#profile-musics').html(data.music_genre);
                                $$('#profile-foods').html(data.food);
                                $$('#profile-drinks').html(data.drink);
                                $$('#profile-places').html(data.hangout_place);
                                if(data.user_id!=userid) {
                                    $$('#unfollow').show();
                                }else{
                                    $$('#unfollow').hide();
                                }
                                $$('#follow').hide();
                                $$('#profile-tabs').show();
                                var html="";
                                $$.each(data.recents, function (k,v) {
                                    var from="";
                                    if(v.link.match(/youtube/)){
                                        var icon = "<i class='fa fa fa-youtube-play color-red' style='font-size:20px'></i>";
                                        var from="youtube";
                                    }else{
                                        var icon = "<i class='fa fa-soundcloud color-orange' style='font-size:20px'></i>";
                                        var from="soundcloud";
                                    }
                                    html += "<li class='item-content' data-title='"+v.title+"' data-songid='"+v.track_id+"' data-from='"+from+"' data-loc='' data-genre='"+v.genre+"' data-link='"+v.link+"' >";
                                    html += "<div class='item-media'>"+icon+"</div>";
                                    html += "<div class='item-inner'><div class='item-title'>"+v.title+"</div>";
                                    html += "<div class='item-after'><i class='fa fa-play-circle' style='font-size:20px'></i></div>";
                                    html += "</div></li>";
                                });
                                $$('#recentsProfile').html(html);
                            }else{
                                $$('#unfollow').hide();
                                $$('#follow').show();
                                $$('#profile-tabs').hide();
                            }
                            $$('#userinfo').show();
                        }
                        window.plugins.spinnerDialog.hide();
                    });
                }
                $$('#back-btn-profile').on('click',function (e) {
                   history.back();
                });
                showProfile(profile_userid,profile_username);
                // myApp.onPageBeforeRemove('search',function (p) {
                //     $$('#menu').show();
                // });
                try{
                    var devicePlatform = device.platform;
                    var socket = io.connect('http://ws.jukebox5d.com/');
                    socket.on("connect",function() {
                        var params={username: username, loc: varLoc, userid: userid};
                        socket.emit("jukeboxLocation", varLoc);
                        socket.emit("getChatFileWeb",params);
                        socket.emit("adduser",params);
                        socket.emit("getRecentSong",{userid: userid, loc: varLoc});
                    });
                    socket.on("playlist",function(resp) {
                        var resp=JSON.parse(resp);
                        var medal="";
                        // alert(JSON.stringify(resp));
                        if(resp.status){
                            //first song
                            if(resp.data.length>0){
                                countRequest=0;
                                if(resp.data[0]){
                                    lastId=resp.data[0].user_id;
                                }
                                for(var i=1;i<resp.data.length;i++){
                                    lastId=resp.data[i].user_id;
                                    if(resp.data[i].user_id==userid) {
                                        countRequest++;
                                    }
                                }
                            }else{
                                countRequest=0;
                                lastId=0;
                            }
                        }
                    });
              }catch (err){
                }
                function jukeDialog(data,reload) {
                    if(countRequest>=2 || lastId==userid || tooFar){
                        if(countRequest>=2 || lastId==userid){
                            myApp.alert("It's not your turn yet. Invite your friend to insert a song first. (max. 2 songs per user)",'Jukebox5D');
                        }else{
                            myApp.alert("Too Far From Admin",'Jukebox5D');
                        }
                    }else{
                        myApp.confirm('Add this song to playlist? <br/> '+data.title,'Jukebox 5D',
                            function () {
                                myApp.prompt('Tell us how you feel about this song!','Jukebox 5D', function (value) {
                                    data.cap=encodeURIComponent(value);
                                    socket.emit("insertSong",data);
                                    lastId=userid;
                                    countRequest++;
                                    myApp.alert('Wait until it is played.','Jukebox5D',function () {
                                        if(reload){
                                            location.href='index.html';
                                        }
                                    });
                                });
                            },
                            function () {

                            }
                        );
                    }
                }
                $$(document).on('click', '#recentsProfile li', function (e) {
                    var data={};
                    data.username = username;
                    data.userid = userid;
                    data.id=$$(this).data('songid');
                    data.title=$$(this).data('title');
                    data.loc=varLoc;
                    data.from=$$(this).data('from');
                    data.link=$$(this).data('link');
                    data.genre=$$(this).data('genre');
                    data.img=encodeURIComponent(myImage);
                    data.flag=0;
                    jukeDialog(data,false);
                });
                $$('#follow').on('click',function (e) {
                    var params={};
                    params.user_id=currentUserIDProfile;
                    params.token= storage.getItem("token");
                    $$.post(follow_url,params, function (data) {
                        showProfile(currentUserIDProfile,currentUsernameProfile);
                    });
                });
                $$('#unfollow').on('click',function (e) {
                    var params={};
                    params.user_id=currentUserIDProfile;
                    params.token= storage.getItem("token");
                    $$.post(unfollow_url,params, function (data) {
                        showProfile(currentUserIDProfile,currentUsernameProfile);
                    });
                });
                function onSuccess(position) {
                    var distance=showDistanceFromServer(position.coords.latitude,position.coords.longitude);
                    if(distance>1000){
                        tooFar=true;
                    }else{
                        tooFar=false;
                    }
                }
                function showDistanceFromServer(latitude, longitude) {
                    var lat2 = lat;
                    var lon2 = long;
                    var R = 6378137; // Radius of the earth in m
                    var dLat = deg2rad(lat2-latitude);  // deg2rad below
                    var dLon = deg2rad(lon2-longitude);
                    var a =
                            Math.sin(dLat/2) * Math.sin(dLat/2) +
                            Math.cos(deg2rad(latitude)) * Math.cos(deg2rad(lat2)) *
                            Math.sin(dLon/2) * Math.sin(dLon/2)
                        ;
                    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                    var d = R * c; // Distance in m
                    console.log(d);
                    return d.toFixed(1);
                }
                function deg2rad(deg) {
                    return deg * (Math.PI/180)
                }

                function onError(error) {
                    console.log('code: '    + error.code    + '\n' +
                        'message: ' + error.message + '\n');
                }

                if (window.cordova) {
                    cordova.plugins.diagnostic.isLocationAvailable(function (available) {
                        if (available) {
                            navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });
                        } else {
                            myApp.alert("Turn On GPS And Restart App !", 'Jukebox5D',function () {
                                cordova.plugins.diagnostic.switchToLocationSettings();

                            });
                        }
                    }, function (error) {
                        console.error("The following error occurred: " + error);
                    });
                }

            }else{
                location.href="choose.html";
            }
        }else{
            location.href="login.html";
        }
    }
};

app.initialize();

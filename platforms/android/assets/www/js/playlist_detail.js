/**
 * Created by Indocyber on 19/08/2017.
 */
window.onerror = function(message, url, lineNumber) {
    alert("Error: " + message + " in " + url + " at line " + lineNumber);
}
$('.tabs').show();
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var messageCount=0;
        var countRequest=0;
        var tooFar=false;
        var lastId=0;
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        if(isLogin=='true') {
            if(locationAdmin!='' && locationAdmin!=null){
                var myApp = new Framework7();
                var $$ = Dom7;
                var userid=storage.getItem('user_id');
                var playlist_id=storage.getItem('playlist_id');
                var username=storage.getItem('username');
                var email=storage.getItem('email');
                var myImage=storage.getItem('image');
                var varLoc=storage.getItem('location_id');
                var latlongTemp=storage.getItem('latlong').split('#');
                var lat=latlongTemp[0];
                var long=latlongTemp[1];
                var mainView = myApp.addView('.view-main', {domCache: true});
                $$('#back-btn').on('click',function (e) {
                    history.back();
                });
                $$("#add-playlist-item").on('click',function () {
                   location.href="search_playlist.html";
                });
                $$('#playlist-name').val(storage.getItem("playlist_name"));
                $$('#title').html(storage.getItem("playlist_name")+"'s Playlist");
                $$('#playlist-name').focus();
                try {
                    var socket = io.connect('http://ws.jukebox5d.com/');
                    socket.on("connect", function () {
                        var params = {username: username, loc: varLoc, userid: userid};
                        socket.emit("jukeboxLocation", varLoc);
                        socket.emit("getChatFileWeb", params);
                        socket.emit("adduser", params);
                        socket.emit("getRecentSong", {userid: userid, loc: varLoc});
                    });
                    socket.on("playlist",function(resp) {
                        var resp=JSON.parse(resp);
                        var medal="";
                        // alert(JSON.stringify(resp));
                        if(resp.status){
                            //first song
                            if(resp.data.length>0){
                                countRequest=0;
                                if(resp.data[0]){
                                    lastId=resp.data[0].user_id;
                                    if(resp.data[0].user_id==userid) {
                                        countRequest++;
                                    }
                                }
                                for(var i=1;i<resp.data.length;i++){
                                    lastId=resp.data[i].user_id;
                                    if(resp.data[i].user_id==userid) {
                                        countRequest++;
                                    }
                                }
                            }else{
                                countRequest=0;
                                lastId=0;
                            }
                        }
                    });
                    getPlaylistItem();
                    function getPlaylistItem() {
                        var params={};
                        params.token=storage.getItem('token');
                        window.plugins.spinnerDialog.show(null,"Loading ...", true);
                        $$.post(get_item_playlist+"/"+playlist_id,params, function (data) {
                            window.plugins.spinnerDialog.hide();
                            var response=JSON.parse(data);
                            if(response.status){
                                var html="";
                                console.log(response.data);
                                for (let i=0;i<response.data.length;i++){
                                    console.log(response.data[i]);
                                    var from="";
                                    if(response.data[i].link.match(/youtube/)){
                                        var icon = "<i class='fa fa fa-youtube-play fa-2x' style='color:#bb0000'></i>";
                                        from="youtube";
                                    }else{
                                        var icon = "<i class='fa fa-soundcloud fa-2x' style='color:#ff3a00'></i>";
                                        from="soundcloud";
                                    }
                                    if(i%2==0){
                                        html += "<li class='item-content' data-title='"+response.data[i].title+"' data-songid='"+response.data[i].track_id+"' data-from='"+from+"'  data-link='"+response.data[i].link+"' style='border-bottom:1px solid #d2d2d2;background-color:#f5f5f5'>";
                                    }else{
                                        html += "<li class='item-content' data-title='"+response.data[i].title+"' data-songid='"+response.data[i].track_id+"' data-from='"+from+"' data-link='"+response.data[i].link+"' style='border-bottom:1px solid #d2d2d2'>";
                                    }

                                    html += "<div class='item-media' style='min-width:30px;max-width:30px'>"+icon+"</div>";
                                    html += "<div class='item-inner'><div class='item-title'>"+response.data[i].title+"</div>";
                                    html += "<div class='item-after' style='text-align:center'>" +
                                        "<i class='fa fa-play-circle fa-2x color-green play-item' data-title='"+response.data[i].title+"' data-songid='"+response.data[i].track_id+"' data-from='"+from+"' data-link='"+response.data[i].link+"' ></i>" +
                                        "&nbsp; &nbsp;<i class='fa fa-remove fa-2x color-red delete-item' data-title='"+response.data[i].title+"' data-songid='"+response.data[i].track_id+"' data-from='"+from+"' data-link='"+response.data[i].link+"' ></i>" +
                                        "</div>";
                                    html += "</div></li>";
                                }
                                console.log(html);
                                $$("#listItem").html(html);
                            }else{
                                $$('#message').html(response.messsage);
                            }
                        },function (error) {
                            window.plugins.spinnerDialog.hide();
                            alert("Network Error");
                        });
                    }
                    $$(document).on('click', '.play-item', function (e) {
                        var data={};
                        data.username = username;
                        data.userid = userid;
                        data.link=$$(this).data('link');
                        data.id=$$(this).data('songid');
                        data.title=$$(this).data('title');
                        data.loc=varLoc;
                        if(data.link.match(/youtube/)){
                            data.from="youtube";
                        }else{
                            data.from="soundcloud";
                        }
                        data.genre="";
                        data.img=encodeURIComponent(myImage);
                        data.flag=1;
                        jukeDialog(data,false);
                    });
                    $$(document).on('click', '.delete-item', function (e) {
                        var data={};
                        data.track_id=$$(this).data('songid');
                        data.title=$$(this).data('title');
                        data.link=$$(this).data('link');
                        data.playlist_id=playlist_id;
                        deleteSong(data);
                    });
                    $$(document).on('click', '#edit-playlist', function (e) {
                        myApp.confirm("Do you want to change playlist name?","Jukebox5D",function (value) {
                            if(value){
                                var params={};
                                params.token=storage.getItem('token');
                                params.playlist_id=playlist_id;
                                params.name=$$('#playlist-name').val();
                                window.plugins.spinnerDialog.show(null,"Loading ...", true);
                                $$.post(update_playlist,params, function (data) {
                                    window.plugins.spinnerDialog.hide();
                                    var response=JSON.parse(data);
                                    if(response.status){
                                        storage.setItem("playlist_name",$$('#playlist-name').val());
                                        $$('#title').html(storage.getItem("playlist_name")+"'s Playlist");
                                        getPlaylistItem();
                                    }else{
                                        $$('#message').html(response.messsage);
                                    }
                                },function (error) {
                                    window.plugins.spinnerDialog.hide();
                                    alert("Network Error");
                                });
                            }
                        });
                    });
                    $$(document).on('click', '#delete-playlist', function (e) {
                        myApp.confirm("Are you sure want to delete this playlist?","Jukebox5D",function (value) {
                            if(value){
                                var params={};
                                params.token=storage.getItem('token');
                                params.playlist_id=playlist_id;
                                window.plugins.spinnerDialog.show(null,"Loading ...", true);
                                $$.post(delete_playlist,params, function (data) {
                                    window.plugins.spinnerDialog.hide();
                                    var response=JSON.parse(data);
                                    if(response.status){
                                        history.back();
                                    }else{
                                        $$('#message').html(response.messsage);
                                    }
                                },function (error) {
                                    window.plugins.spinnerDialog.hide();
                                    alert("Network Error");
                                });
                            }
                        });
                    });
                    function deleteSong(data) {
                        myApp.confirm("Are you sure want to delete this song?","Jukebox5D",function (value) {
                            if(value){
                                var params=data;
                                params.token=storage.getItem('token');
                                window.plugins.spinnerDialog.show(null,"Loading ...", true);
                                $$.post(delete_item_playlist,params, function (data) {
                                    window.plugins.spinnerDialog.hide();
                                    var response=JSON.parse(data);
                                    if(response.status){
                                        getPlaylistItem();
                                    }else{
                                        $$('#message').html(response.messsage);
                                    }
                                },function (error) {
                                    window.plugins.spinnerDialog.hide();
                                    alert("Network Error");
                                });
                            }
                        });
                    }
                    function jukeDialog(data,reload) {
                        if(countRequest>=2 || lastId==userid || tooFar){
                            if(countRequest>=2 || lastId==userid){
                                myApp.alert("It's not your turn yet. Invite your friend to insert a song first. (max. 2 songs per user)",'Jukebox5D');
                            }else{
                                myApp.alert("Too Far From Admin",'Jukebox5D');
                            }
                        }else{
                            myApp.confirm('Add this song to playlist? <br/> '+data.title,'Jukebox 5D',
                                function () {
                                    myApp.prompt('Tell us how you feel about this song!','Jukebox 5D', function (value) {
                                        data.cap=encodeURIComponent(value);
                                        socket.emit("insertSong",data);
                                        lastId=userid;
                                        countRequest++;
                                        myApp.alert('Wait until it is played.','Jukebox5D',function () {
                                            if(reload){
                                                location.href='index.html';
                                            }
                                        });
                                    });
                                },
                                function () {

                                }
                            );
                        }
                    }
                    function onSuccess(position) {
                        var distance=showDistanceFromServer(position.coords.latitude,position.coords.longitude);
                        if(distance>1000){
                            tooFar=true;
                        }else{
                            tooFar=false;
                        }
                    }
                    function showDistanceFromServer(latitude, longitude) {
                        var lat2 = lat;
                        var lon2 = long;
                        var R = 6378137; // Radius of the earth in m
                        var dLat = deg2rad(lat2-latitude);  // deg2rad below
                        var dLon = deg2rad(lon2-longitude);
                        var a =
                                Math.sin(dLat/2) * Math.sin(dLat/2) +
                                Math.cos(deg2rad(latitude)) * Math.cos(deg2rad(lat2)) *
                                Math.sin(dLon/2) * Math.sin(dLon/2)
                            ;
                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                        var d = R * c; // Distance in m
                        console.log(d);
                        return d.toFixed(1);
                    }
                    function deg2rad(deg) {
                        return deg * (Math.PI/180)
                    }

                    function onError(error) {
                        console.log('code: '    + error.code    + '\n' +
                            'message: ' + error.message + '\n');
                    }

                    if (window.cordova) {
                        cordova.plugins.diagnostic.isLocationAvailable(function (available) {
                            if (available) {
                                navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 30000 });
                            } else {
                                myApp.alert("Turn On GPS And Restart App !", 'Jukebox5D',function () {
                                    cordova.plugins.diagnostic.switchToLocationSettings();

                                });
                            }
                        }, function (error) {
                            console.error("The following error occurred: " + error);
                        });
                    }

                }catch (err){

                }
            }else{
                location.href="choose.html";
            }
        }else{
            location.href="login.html";
        }
    }
};

app.initialize();

/**
 * Created by mrkai303 on 01/12/16.
 */
var app = {
    initialize: function() {
        var storage = window.localStorage;
        var myApp = new Framework7();
        var $$ = Dom7;
        var mainView = myApp.addView('.view-main', {domCache: true});
        var isLogin = storage.getItem("login");
        if(isLogin!='true') {
            function parseResponse(response) {
                var response = JSON.parse(response);
                if (response.status) {
                    storage.setItem('login', 'true');
                    storage.setItem("user_id", response.data.user_id);
                    storage.setItem("username", response.data.username);
                    storage.setItem("email", response.data.email);
                    storage.setItem("image", response.data.twitter_image);
                    storage.setItem("token", response.token);
                    storage.setItem("register_as", response.data.register_as);
                    if(response.data.bio==null){
                        response.data.bio="";
                    }
                    var blank=[];
                    storage.setItem("bio", response.data.bio);
                    if(response.data.food==null){
                        storage.setItem("food", "[]");
                    }else{
                        storage.setItem("food", response.data.food);
                    }
                    if(response.data.hangout_place==null){
                        storage.setItem("hangout_place", "");
                    }else{
                        storage.setItem("hangout_place", response.data.hangout_place);
                    }
                    if(response.data.drink==null){
                        storage.setItem("drink", "");
                    }else{
                        storage.setItem("drink", response.data.drink);
                    }
                    var music_json = {};
                    if (response.data.music_genre == "" || response.data.music_genre == null) {
                        storage.setItem("music_genre", "[]");
                    } else {
                        var musics = JSON.parse(response.data.music_genre);
                        storage.setItem("music_genre", JSON.stringify(musics));
                    }
                    location.href = "choose.html";
                } else {
                    myApp.alert(response.message,'Jukebox5D');
                }
                window.plugins.spinnerDialog.hide();
            }

            $$('#LoginFB').on('click', function (e) {
                facebookConnectPlugin.login(["public_profile", "email"], function (response) {
                    if (response.status) {
                        facebookConnectPlugin.api('/me?fields=id,name,email,picture', ["public_profile", "email"], function (data) {
                            window.plugins.spinnerDialog.show(null, "Loading ...", true);
                            $$.post(sosmed_url, {
                                email: data.email,
                                name: data.name,
                                image: data.picture.data.url
                            }, function (response) {
                                parseResponse(response);
                            }, function (error) {
                                alert("Network Error");
                                window.plugins.spinnerDialog.hide();
                            });
                        }, function (reason) {
                            alert("failed to get data");
                            window.plugins.spinnerDialog.hide();
                        });

                    } else {
                        alert("failed to authenticated");
                        window.plugins.spinnerDialog.hide();
                    }
                }, function (error) {
                    // alert("");
                });
            });
            $$('#register').on('click', function (e) {
                location.href = "register.html";
            });
            $$('#forgot').on('click', function (e) {
                location.href = "forgot.html";
            });
            $$('#Login').on('click', function (e) {
                window.plugins.spinnerDialog.show(null, "Loading ...", true);
                $$.post(login_url, {
                    email: $$('#email').val(),
                    password: $$('#password').val()
                }, function (response) {
                    parseResponse(response);
                }, function (error) {
                    alert("Network Error");
                    window.plugins.spinnerDialog.hide();
                });
            });
        }else{
            location.href='choose.html';
        }
    },
    onDeviceReady: function() {

    }
};

app.initialize();
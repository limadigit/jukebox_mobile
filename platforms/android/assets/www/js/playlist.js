/**
 * Created by Indocyber on 19/08/2017.
 */
window.onerror = function(message, url, lineNumber) {
    alert("Error: " + message + " in " + url + " at line " + lineNumber);
}
$('.tabs').show();
Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};
var app = {
    initialize: function() {
        document.addEventListener('deviceready',this.onDeviceReady);
    },
    onDeviceReady: function() {
        var storage = window.localStorage;
        var isLogin = storage.getItem("login");
        var locationAdmin = storage.getItem("location");
        if(isLogin=='true') {
            if(locationAdmin!='' && locationAdmin!=null){
                var myApp = new Framework7();
                var $$ = Dom7;
                var userid=storage.getItem('user_id');
                var username=storage.getItem('username');
                var email=storage.getItem('email');
                var myImage=storage.getItem('image');
                var varLoc=storage.getItem('location_id');
                var latlongTemp=storage.getItem('latlong').split('#');
                var lat=latlongTemp[0];
                var long=latlongTemp[1];
                var mainView = myApp.addView('.view-main', {domCache: true});
                $$('#back-btn').on('click',function (e) {
                    history.back();
                });
                $$("#add-playlist").on('click',function (e) {
                   myApp.prompt("Enter library name","Jukebox5D",function (value) {
                       if(value!=""){
                           window.plugins.spinnerDialog.show(null, "Loading ...", true);
                           $$.post(save_playlist, {
                               playlist_name: value,
                               token: storage.getItem('token')
                           }, function (response) {
                              window.plugins.spinnerDialog.hide();
                              var json=JSON.parse(response);
                              if(json.status){
                                  getPlaylist()
                              }else{
                                  myApp.alert(json.message,"Jukebox5D");
                              }
                           }, function (error) {
                               alert("Network Error");
                               window.plugins.spinnerDialog.hide();
                           });
                       }else {
                           myApp.alert("Please, enter library name !","Jukebox5D");
                       }
                   });
                });
                getPlaylist();
                function getPlaylist() {
                    var params={};
                    params.token=storage.getItem('token');
                    window.plugins.spinnerDialog.show(null,"Loading ...", true);
                    $$.get(get_playlist,params, function (data) {
                        window.plugins.spinnerDialog.hide();
                        var response=JSON.parse(data);
                        if(response.status){
                            var html="";
                            for (var i=0;i<response.data.length;i++){
                               html+=' <li> <a href="#"  data-playlistname="'+response.data[i].playlist_name+'" data-playlistid="'+response.data[i].playlist_id+'" class="item-link item-content">' +
                                   '<div class="item-inner">'+
                                   '<div class="item-title">'+response.data[i].playlist_name+'</div>'+
                                   '</div></a>'+
                                   '</li>';
                            }
                            if(response.data.length<1){
                              html += 'You have no Library';
                            }
                            $$("#playlist-items").html(html);
                            $$('#message').html("");
                        }else{
                            $$('#message').html(response.messsage);
                        }
                    },function (error) {
                        window.plugins.spinnerDialog.hide();
                        alert("Network Error");
                    });
                }
                $$('#playlist-items').on('click','li a',function (event) {
                    var playlist_id=$$(this).data('playlistid');
                    var playlist_name=$$(this).data('playlistname');
                    storage.setItem("playlist_id",playlist_id);
                    storage.setItem("playlist_name",playlist_name);
                    location.href="detail_playlist.html";
                });
            }else{
                location.href="choose.html";
            }
        }else{
            location.href="login.html";
        }
    }
};

app.initialize();
